//
//  Double.swift
//  BDG
//
//  Created by Pınar Biltekin on 31.03.2021.
//

import Foundation

extension Double {
    func getDateStringFromUTC() -> String {
        
        let date = NSDate(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium //Set time style
        dateFormatter.dateStyle = .medium //Set date style
        dateFormatter.timeZone = NSTimeZone() as TimeZone
        return dateFormatter.string(from: date as Date)
    }
    
    func getHourStringFromUTC() -> String {
        
        let date = NSDate(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium //Set time style
        dateFormatter.dateStyle = .medium //Set date style
        dateFormatter.timeZone = NSTimeZone() as TimeZone
        return Date.getHourFrom(date: date as Date)
    }
    
    
}

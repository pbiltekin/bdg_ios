//
//  RightNowView.swift
//  BDG
//
//  Created by Pınar Biltekin on 30.03.2021.
//

import Foundation
import UIKit

class RightNowView: UIView {

    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var tempatureLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var cityCountryLabel: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    
    
    
    func clear() {
        sunsetLabel.text = ""
        sunriseLabel.text = ""
        tempatureLabel.text = ""
        windDirectionLabel.text = ""
        weatherDescription.text = ""
        cityCountryLabel.text = ""
    }
    
    func kelvinToCelciusText (temp: Double)-> String{
        return String(format: "%.0f", temp - 273.15)
    }
    
    func fahrenheittoCelsiusText (temp: Double)-> String{
        return String(format: "%.0f", (temp-32.0) / 1.8)
    }
    
    
    func updateView(currentWeather: Current, city: String, country: String) {
        
        sunsetLabel.text = "Gün Batımı: " + (currentWeather.sunset).getHourStringFromUTC()
        sunriseLabel.text = "Gün Doğumu: " + (currentWeather.sunrise).getHourStringFromUTC()
        tempatureLabel.text = "Hava Sıcaklığı: " + fahrenheittoCelsiusText(temp: currentWeather.temp)
        windDirectionLabel.text = "Rüzgar: " +  String(format: "%.0f", currentWeather.wind_deg) + " " + String(format: "%.0f", currentWeather.wind_speed)
        cityCountryLabel.text = city
        weatherDescription.text = currentWeather.weather[0].description
        
    }

}

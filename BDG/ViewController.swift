//
//  ViewController.swift
//  BDG
//
//  Created by Pınar Biltekin on 29.03.2021.
//

import UIKit
import MapKit



class ViewController: UIViewController, CLLocationManagerDelegate {

    var city = ""
    var country = ""
    var weatherResult: Result?
    var locationManger: CLLocationManager!
    var currentlocation: CLLocation?
    var hud: MBProgressHUD = MBProgressHUD()
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var rightNowView: RightNowView!
    @IBOutlet weak var weatherDetailView: WeatherDetailView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearAll()
        getLocation()
        self.activityIndicator.startAnimating()
    }
    
    func getWeather() {
        OpenWeatherServices.shared.getWeather(onSuccess: { (result) in
            self.weatherResult = result
            self.updateViews()
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }) { (errorMessage) in
            debugPrint(errorMessage)
        }
    }
    
    func clearAll() {
        rightNowView.clear()
        
        //weatherDetailView.clear()
    }
    
    func getLocation() {
       
        if (CLLocationManager.locationServicesEnabled()) {
            locationManger = CLLocationManager()
            locationManger.delegate = self
            locationManger.desiredAccuracy = kCLLocationAccuracyBest
            locationManger.requestWhenInUseAuthorization()
            locationManger.requestLocation()
        }
        
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.currentlocation = location
            
            let latitude: Double = self.currentlocation!.coordinate.latitude
            let longitude: Double = self.currentlocation!.coordinate.longitude
            
            OpenWeatherServices.shared.setLatitude(latitude)
            OpenWeatherServices.shared.setLongitude(longitude)
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                }
                if let placemarks = placemarks {
                    if placemarks.count > 0 {
                        let placemark = placemarks[0]
                        if let city = placemark.locality {
                            self.city = city
                        }
                        if let country = placemark.locality{
                            self.country = country
                        }
                    }
                }
            }
          
                getWeather()
            
        }
    }
    
    func updateViews() {
        updateTopView()
      //  updateBottomView()
    }
    
    func updateTopView() {
        guard let weatherResult = weatherResult else {
            return
        }
        
        rightNowView.updateView(currentWeather: weatherResult.current, city: city, country: country)
    }
    
    func updateBottomView() {
        guard let weatherResult = weatherResult else {
            return
        }
        
      //  let title = weatherDetailView.getSelectedTitle()
        
        if title == "Today" {
            weatherDetailView.updateViewForToday(weatherResult.hourly)
        } else if title == "Weekly" {
            weatherDetailView.updateViewForWeekly(weatherResult.daily)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugPrint(error.localizedDescription)
    }
    
    @IBAction func getWeatherTapped(_ sender: UIButton) {
        clearAll()
        getLocation()
    }
    
    @IBAction func todayWeeklyValueChanged(_ sender: UISegmentedControl) {
        clearAll()
        updateViews()
    }


}

